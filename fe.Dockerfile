ARG FE_BASE_IMAGE_BUILD_STAGE

ARG FE_RUN_IMAGE

FROM ${FE_BASE_IMAGE_BUILD_STAGE} as build-stage

ENV APP_DIR="/fe"

WORKDIR $APP_DIR

COPY . .

# Run any build scripts or commands here, add or replace the commands as needed
RUN npm install

RUN npm run build

# image must be nginx:<suitable_version>
FROM ${FE_RUN_IMAGE} as deploy-stage

COPY --from=build-stage $APP_DIR/dist /usr/share/nginx/html

# if the port needs to be changed, please change the pipeline accordingly
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
