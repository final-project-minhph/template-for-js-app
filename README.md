# Template for JS app


## What is this?
- This is the template for which u can use to create a software product with monolithic architecture and the auto-op platform will do the deploy part.   

## How to use the template and get the project up and running
- Clone this repo to your local
- Replace env variables with your own:
    + With frontend part: use the fe.Dockerfile
    + With backend part: use the be.Dockerfile
    + Config paths for components as "<component>.<student_id>.minhph.website"
    + The connection between components should be declared clearly using envs 
- Register the student id with the admin, then push it with the naming convention of: fe/be.student-id
- The product will be automatically deployed soon
