ARG BE_IMAGE

FROM ${BE_IMAGE}

WORKDIR /be

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE ${BE_PORT}

CMD [ "npm", "start" ]
